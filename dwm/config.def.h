/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

// custome
static const char *volup[] = {"pw-volume", "change", "+10%", NULL};
static const char *voldown[] = {"pw-volume", "change", "-10%", NULL};
static const char *mute[] = {"mute-volume", NULL};
static const char *brup[] = {"xbacklight", "-inc", "10", NULL};
static const char *brdown[] = {"xbacklight", "-dec", "10", NULL};
static const char *menu[] = {"rofi", "-show", "drun", NULL};
static const char *qb[] = {"qutebrowser", NULL};
static const char *ss[] = {"flameshot", "gui", NULL};
static const char *fm[] = {"pcmanfm", NULL};
static const char *nt[] = {"n-translate", NULL};

/* appearance */
static const unsigned int borderpx  = 0;        /* border pixel of windows */
static const unsigned int gappx     = 5;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft = 0;   	/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 1;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;     /* 0 means no bar */
static const int user_bh            = 24;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const int topbar             = 1;     /* 0 means bottom bar */
static const int horizpadbar        = 1;
static const int vertpadbar         = 7;
static const char *fonts[]          = { "SFMono Nerd Font:size=9:style=bold:antialias=true:autohint=true",
                                        "EPSON 正楷書体Ｍ:size=9.5:style=bold:antialias=true:autohint=true",
                                        "JetBrainsMono Nerd Font:size=11:antialias=true:autohint=true",
                                        "Noto Color Emoji:pixelsize=10:antialias=true:autohint=true"};
static const char col_bg_def[]     = "#131020";
static const char col_borderbar[]   = "#131020";
static const char col_fg[]          = "#D9E0EE";
static const char col_yellow[]      = "#FAE3B0";
static const char col_green[]       = "#ABE9B3";
static const char col_red[]         = "#F28FAD";
static const char col_blue[]        = "#96CDFB";
static const char col_peach[]       = "#F8BD96";
static const char col_overlay[]       = "#9399b2";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
        [SchemeNorm] = { col_overlay, col_bg_def, col_bg_def },
        [SchemeSel]  = { col_overlay, col_bg_def,  col_bg_def  },
};

/* tagging */
/*static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" }; */
static const char *tags[] = { "一", "二", "三", "四", "五", "六", "七", "八", "九"};

static const unsigned int ulinepad	= 5;	/* horizontal padding between the underline and tag */
static const unsigned int ulinestroke	= 2;	/* thickness / height of the underline */
static const unsigned int ulinevoffset	= 3;	/* how far above the bottom of the bar the line should appear */
static const int ulineall 		= 0;	/* 1 to show underline on all tags, 0 for just the active ones */

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                      instance    title       tags mask     isfloating   monitor */
    {"SimpleScreenRecorder",    NULL,       NULL,       1 << 8,        1,           -1},
    {"fontforge",               NULL,       NULL,       1 << 6,        0,           -1},
    {"Gimp-2.10",               NULL,       NULL,       1 << 5,        0,           -1},
    {"Inkscape",                NULL,       NULL,       1 << 4,        0,           -1},
    {"qutebrowser",             NULL,       NULL,       1 << 3,        0,           -1},
    {"Firefox",                 NULL,       NULL,       1 << 3,        0,           -1},
    {"TelegramDesktop",         NULL,       NULL,       1 << 2,        0,           -1},
    {"Pcmanfm",                 NULL,       NULL,       1 << 1,        0,           -1},
    {"Firefox",                 NULL,  "Library",       0,             1,           -1},
    {"mpv",                     NULL,       NULL,       0,             1,           -1},
    {"System-config-printer.py",NULL,       NULL,       0,             1,           -1},
    {"Manage_extensions.py",    NULL,       NULL,       0,             1,           -1},
    {"Lxappearance",            NULL,       NULL,       0,             1,           -1},
    {"qt5ct",                   NULL,       NULL,       0,             1,           -1},
    {"Sxiv",                    NULL,       NULL,       0,             1,           -1},
    {"Xarchiver",               NULL,       NULL,       0,             1,           -1},
    {"Nitrogen",                NULL,       NULL,       0,             1,           -1},
    {"Gucharmap",               NULL,       NULL,       0,             1,           -1},
    {"Gcolor2",                 NULL,       NULL,       0,             1,           -1},
    {"Galculator",              NULL,       NULL,       0,             1,           -1},
    {"Rofi",                    NULL,       NULL,       0,             1,           -1},
    {"Ankama Launcher",         NULL,       NULL,       0,             1,           -1},
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function
	{ " ",      tile },
	{ "",      tile }, */
	{ "",      tile }, 
/*	{ "><>",      NULL },    
	{ "[M]",      monocle }, */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *termcmd[]  = { "kitty", "-T", "Terminal", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{0,              XF86XK_AudioMute,         spawn,          {.v = mute}},
	{0,              XF86XK_AudioRaiseVolume,  spawn,          {.v = volup}},
	{0,              XF86XK_AudioLowerVolume,  spawn,          {.v = voldown}},
	{0,              XF86XK_MonBrightnessUp,   spawn,          {.v = brup}},
  {0,              XF86XK_MonBrightnessDown, spawn,          {.v = brdown}},
	{0,                             XK_Print,  spawn,          {.v = ss}},
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd }},
	{ MODKEY,                       XK_m,      spawn,          {.v = menu }},
	{ MODKEY,                       XK_space,  spawn,          {.v = qb}},
  { MODKEY,                       XK_f,      spawn,          {.v = fm}},
  { MODKEY,                       XK_t,      spawn,          {.v = nt}},
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
/*	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} }, */
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

