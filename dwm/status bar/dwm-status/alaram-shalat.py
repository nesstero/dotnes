#! /usr/bin/python

from jadwal_shalat import shalat
from datetime import datetime as date
# from time import sleep
# from random import choice as acak

class waktuShalat:
    def __init__(self):
        jadwal = shalat()
        self.shalat = jadwal.jadwal()

    def waktu(self, waktu):
        shalat = {
                "Imsak" : self.shalat[2],
                "Subuh" : self.shalat[3],
                "Terbit" : self.shalat[4],
                "Dhuha" : self.shalat[5],
                "Dzuhur" : self.shalat[6],
                "Ashar" : self.shalat[7],
                "Maghrib" : self.shalat[8],
                "Isya" : self.shalat[9],
                }
        return shalat[waktu]

    def timer(self, sekarang, waktu):
        x = sekarang
        y = waktu
        x = x.split(":")
        x = int(x[0])*60 + int(x[1])
        y = y.split(":")
        y = int(y[0])*60 + int(y[1])
        z = y - x
        jam = z / 60
        menit = z % 60
        if int(jam) < 10:
            jam = f"0{int(jam)}"
        else:
            jam = int(jam)
        if menit < 10:
            menit = f"0{menit}"
        return f"{jam}:{menit}"

    # def shalat():
    #     sleep(120)
    #     shalat = ["Dahsyatnya Sujud", "Obat Galau", "Amalan Yang Paling Baik", "Sebagai Penolong", "Yang Pertama Kali DiHisab", "Kemenangan", "Penghapus Dosa", "Indah", "Ketenangan Hati", "Pertolongan Terbesar"]
    #     shalat = acak(shalat)
    #     return shalat

    def getWaktu(self, sekarang):
        shalat = ["Subuh", "Dhuha", "Dzuhur", "Ashar", "Maghrib", "Isya"]
        waktu = "Dahsyatnya Sujud"
        for s in shalat:
            if sekarang == self.waktu(s):
                waktu = f"Waktu Shalat {s}"
            elif sekarang >= "00:00" and sekarang <= "03:00":
                waktu = "Jangan Lupa Tahajud"
            elif sekarang >= "03:00" and sekarang < self.waktu("Imsak"):
                timer = self.timer(sekarang, self.waktu("Imsak"))
                waktu = f"Imsak {timer}"
            elif sekarang == self.waktu("Imsak"):
                waktu = "Imsak, Lekas Sahur"
            elif sekarang > self.waktu("Subuh") and sekarang < self.waktu("Dhuha"):
                timer = self.timer(sekarang, self.waktu("Terbit"))
                waktu = f"Terbit {timer}"
            else:
                if sekarang > self.waktu("Imsak") and sekarang < self.waktu("Subuh"):
                    timer = self.timer(sekarang, self.waktu("Subuh"))
                    waktu = f"Subuh {timer}"
                else:
                    for w in range(0, 5):
                        if sekarang > self.waktu(shalat[w]) and sekarang < self.waktu(shalat[w+1]):
                            timer = self.timer(sekarang, self.waktu(shalat[w+1]))
                            waktu = f"{shalat[w+1]} {timer}"
        return waktu

jam = date.now()
waktu = waktuShalat()
waktu = waktu.getWaktu(jam.strftime("%R"))
if waktu == "":
    print(f" ^c#161320^^b#9399b2^  {waktu} ^b#161320^")
else:
    print(f" ^c#161320^^b#9399b2^  {waktu} ^b#161320^")
