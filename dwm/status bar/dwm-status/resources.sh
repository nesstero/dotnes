#!/usr/bin/dash
kernel() {
  stat=$(uname -r | rev | cut -b3- | rev | cut -b1-)
  printf "^c#9399b2^  $stat "
}

logo() {
  printf "  "
}

mem() {
  memused=$(free -h --giga | grep Mem | awk '{print $3}')
  
  printf "^c#161320^^b#9399b2^ ﬙ $memused "
  printf "^b#161320^ "
}

temp_cpu() {
  crit=50

  read -r temp </sys/class/thermal/thermal_zone0/temp
  temp="${temp%???}"

  if [ "$temp" -lt "$crit" ] ; then
      printf "^c#161320^^b#9399b2^  $temp° "
      printf "^b#161320^ "
  else
      printf "^c#161320^^b#9399b2^  $temp° "
      printf "^b#161320^ "
  fi
}

vol() {
  vol=$(pw-volume status | awk '{print $1}' | rev | cut -b2- | rev | cut -b15-)
  status=$(pw-volume status | awk '{print $1}' | rev | cut -b3- | rev | cut -b9-)

  if [ "$status" = "mute" ]; then
      printf "^c#161320^^b#9399b2^  "
      printf "^b#161320^ "
  else
      printf "^c#161320^^b#9399b2^  $vol "
      printf "^b#161320^ "
  fi
}

light() {
  printf "^c#161320^^b#9399b2^  $(xbacklight -get) "
  printf "^b#161320^"
}

calendar() {
  printf "^c#9399b2^$(date '+ %A%t%d%t%b%t %R')"
}

bat() {
  prec=$(upower -i `upower -e | grep 'BAT'` | grep 'percentage:' | awk '{print $2}')
  stat=$(upower -i `upower -e | grep 'BAT'` | grep 'state:' | awk '{print $2}')

  if [ "$stat" = "charging" ]; then
	  printf "^c#9399b2^  "
  else
    printf "^c#9399b2^  $prec%"
  fi
}

connection() {
   status=$(wpa_cli status | sed -n '/wpa_state/s/^.*=//p')

   case $status in
      'COMPLETED')
         printf "^c#9399b2^  "
         ;;
      'INTERFACE_DISABLED')
         printf "^c#9399b2^  "
         ;;
      'SCANNING')
         printf "^c#9399b2^  "
         ;;
      'AUTHENTICATING')
         printf "^c#9399b2^  "
         ;;
   esac
}

mem
temp_cpu
vol
light
$HOME/.local/bin/dwm-status/alaram-shalat.py
calendar
bat
connection
